# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:23+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita kbd Enter guilabel\n"

#: ../../reference_manual/tools/crop.rst:1
msgid "Krita's crop tool reference."
msgstr "A referência da ferramenta de recorte do Krita."

#: ../../reference_manual/tools/crop.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/crop.rst:11
msgid "Crop"
msgstr "Recortar"

#: ../../reference_manual/tools/crop.rst:11
msgid "Trim"
msgstr "Recorte"

#: ../../reference_manual/tools/crop.rst:16
msgid "Crop Tool"
msgstr "Ferramenta de Recorte"

#: ../../reference_manual/tools/crop.rst:18
msgid ""
"The crop tool can be used to crop an image or layer. To get started, choose "
"the :guilabel:`Crop` tool and then click once to select the entire canvas. "
"Using this method you ensure that you don't inadvertently grab outside of "
"the visible canvas as part of the crop. You can then use the options below "
"to refine your crop. Press the :kbd:`Enter` key to finalize the crop action, "
"or use the :guilabel:`Crop` button in the tool options docker."
msgstr ""
"A ferramenta de recorte poderá ser usada para recortar uma imagem ou camada. "
"Para começar, escolha a ferramenta :guilabel:`Recortar` e depois carregue "
"uma vez para seleccionar a área de desenho inteira. Se usar este método, "
"garante que não irá capturar nada fora da área de desenho visível como parte "
"do recorte. Poderá então usar as opções abaixo para ajustar o seu recorte. "
"Carregue em :kbd:`Enter` para concluir a acção ou usar o botão :guilabel:"
"`Recortar` na área de opções da ferramenta."

#: ../../reference_manual/tools/crop.rst:20
msgid ""
"At its most basic, the crop tool allows you to size a rectangle around an "
"area and reduce your image or layer to only that content which is contained "
"within that area. There are several options which give a bit more "
"flexibility and precision."
msgstr ""
"No seu modo mais básico, a ferramenta de recorte permite-lhe definir o "
"tamanho de um rectângulo em torno de uma dada área e reduzir a sua imagem ou "
"camada apenas ao conteúdo dentro dessa área. Existem diversas opções que lhe "
"dão um pouco mais de flexibilidade e precisão."

#: ../../reference_manual/tools/crop.rst:22
msgid ""
"The two numbers on the left are the exact horizontal position and vertical "
"position of the left and top of the cropping frame respectively. The numbers "
"are the right are from top to bottom: width, height, and aspect ratio. "
"Selecting the check boxes will keep any one of these can be locked to allow "
"you to manipulate the other two without losing the position or ratio of the "
"locked property."
msgstr ""
"Os dois números à esquerda são a posição horizontal e vertical exactas do "
"extremo esquerdo e superior da área de recorte, respectivamente. Os números "
"à direita são a largura, altura e as proporções de tamanho. Se assinalar as "
"opções de marcação, irá manter qualquer uma dessas variáveis bloqueadas para "
"lhe permitir manipular as outras duas sem perder a posição ou as proporções "
"da propriedade bloqueada."

#: ../../reference_manual/tools/crop.rst:24
msgid "Center"
msgstr "Centro"

#: ../../reference_manual/tools/crop.rst:25
msgid "Keeps the crop area centered."
msgstr "Mantém a área de recorte centrada."

#: ../../reference_manual/tools/crop.rst:26
msgid "Grow"
msgstr "Crescer"

#: ../../reference_manual/tools/crop.rst:27
msgid "Allows the crop area to expand beyond the image boundaries."
msgstr "Permite expandir a área de recorte para além dos limites da imagem."

#: ../../reference_manual/tools/crop.rst:28
msgid "Applies to"
msgstr "Aplica-se a"

#: ../../reference_manual/tools/crop.rst:29
msgid ""
"Lets you apply the crop to the entire image or only to the active layer. "
"When you are ready, hit the :guilabel:`Crop` button and the crop will apply "
"to your image."
msgstr ""
"Permite-lhe aplicar o recorte a toda a imagem ou apenas à camada activa. "
"Quando estiver pronto, carregue no botão :guilabel:`Recortar` para aplicar o "
"recorte à sua imagem."

#: ../../reference_manual/tools/crop.rst:31
msgid "Decoration"
msgstr "Decoração"

#: ../../reference_manual/tools/crop.rst:31
msgid ""
"Help you make a composition by showing you lines that divide up the screen. "
"You can for example show thirds here, so you can crop your image according "
"to the `Rule of Thirds <https://en.wikipedia.org/wiki/Rule_of_thirds>`_."
msgstr ""
"Ajuda-o a criar uma composição, apresentando-lhe as linhas que dividem o "
"ecrã. Poderá, por exemplo, mostrar aqui os terços, para que possa recortar a "
"sua imagem de acordo com a Regra dos Terços <https://en.wikipedia.org/wiki/"
"Rule_of_thirds>`_."

#: ../../reference_manual/tools/crop.rst:34
msgid "Continuous Crop"
msgstr "Recorte Contínuo"

#: ../../reference_manual/tools/crop.rst:36
msgid ""
"If you crop an image, and try to start a new one directly afterwards, Krita "
"will attempt to recall the previous crop, so you can continue it. This is "
"the *continuous crop*. You can press the :kbd:`Esc` key to cancel this and "
"crop anew."
msgstr ""
"Se recortar uma imagem e tentar iniciar um novo logo a seguir, o Krita irá "
"tentar invocar o recorte anterior, para que o possa prosseguir. Este é o "
"*recorte contínuo*. Poderá carregar em :kbd:`Esc` para cancelar este recorte "
"e iniciar um novo."
