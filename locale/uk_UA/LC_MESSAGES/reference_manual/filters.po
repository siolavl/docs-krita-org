# Translation of docs_krita_org_reference_manual___filters.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-16 07:13+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/filters.rst:5
msgid "Filters"
msgstr "Фільтри"

#: ../../reference_manual/filters.rst:7
msgid ""
"Filters are little scripts or operations you can run on your drawing. You "
"can visualize them as real-world camera filters that can make a photo darker "
"or blurrier. Or perhaps like a coffee filter, where only water and coffee "
"gets through, and the ground coffee stays behind."
msgstr ""
"Фільтри є невеличкими скриптами або діями, які ви можете виконувати над "
"вашим малюнком. Ви можете візуально уявити їх як фільтри звичайного "
"фотоапарата, які можуть зробити знімок темнішим чи розмитішим. Або, "
"наприклад, уявити їх як кавовий фільтр, який пропускає лише воду і каву із "
"не пропускає кавову гущу."

#: ../../reference_manual/filters.rst:9
msgid ""
"Filters are unique to digital painting in terms of complexity, and their "
"part of the painting pipeline. Some artists only use filters to adjust their "
"colors a little. Others, using Filter Layers and Filter Masks use them to "
"dynamically update a part of an image to be filtered. This way, they can "
"keep the original underneath without changing the original image. This is a "
"part of a technique called 'non-destructive' editing."
msgstr ""
"Фільтри займають унікальне місце у цифровому малюванні з точки зору "
"складності та їхнього місця у процедурах малювання. Частина художників "
"використовує фільтри лише для незначного коригування кольорів. Інші "
"використовують шари фільтрування та маски фільтрування для динамічного "
"оновлення частини зображення шляхом фільтрування. У такий спосіб можна "
"зберігати початкове зображення у нижньому шарі без внесення змін до нього. "
"Це усе є частиною методики, яка називається «неруйнівним» редагуванням."

#: ../../reference_manual/filters.rst:11
msgid ""
"Filters can be accessed via the :guilabel:`Filters` menu. Krita has two "
"types of filters: Internal and G'MIC filters."
msgstr ""
"Доступ до фільтрів можна отримати за допомогою меню :guilabel:`Фільтри`. У "
"Krita передбачено два типи фільтрів: вбудовані фільтри і фільтри G'MIC."

#: ../../reference_manual/filters.rst:13
msgid ""
"Internal filters are often multithreaded, and can thus be used with the "
"filter brush or the adjustment filters."
msgstr ""
"Вбудовані фільтри часто працюють у декілька потоків, тому можуть бути "
"використані разом із пензлем фільтрування або фільтрами коригування."
