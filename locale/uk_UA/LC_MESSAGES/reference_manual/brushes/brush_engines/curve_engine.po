# Translation of docs_krita_org_reference_manual___brushes___brush_engines___curve_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___curve_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 13:38+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-4.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-4.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:1
msgid "The Curve Brush Engine manual page."
msgstr "Розділ підручника щодо рушія криволінійних пензлів."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:18
msgid "Curve Brush Engine"
msgstr "Рушій криволінійних пензлів"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:13
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:21
msgid ".. image:: images/icons/curvebrush.svg"
msgstr ".. image:: images/icons/curvebrush.svg"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:22
msgid ""
"The curve brush is a brush engine which creates strokes made of evenly "
"spaced lines. It has, among other things been used as a replacement for "
"pressure sensitive strokes in lieu of a tablet."
msgstr ""
"Пензель криволінійного малювання — рушій пензлів, який створює штрихи, які "
"складаються із рівномірно розподілених ліній. Ним можна скористатися, серед "
"інших речей, як замінником мазків із чутливістю до тиску, якщо у вас немає "
"планшета."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:25
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:27
msgid ""
"First off, the line produced by the Curve brush is made up of 2 sections:"
msgstr ""
"Перш за все, лінія, яку малює криволінійний пензель, складається з 2 частин:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:29
msgid "The connection line, which is the main line drawn by your mouse"
msgstr "Лінії з'єднання, яка є основною лінією, яку малює вказівник миші."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:31
msgid ""
"The curve lines I think, which are the extra fancy lines that form at "
"curves. The curve lines are formed by connecting one point of the curve to a "
"point earlier on the curve. This also means that if you are drawing a "
"straight line, these lines won't be visible, since they'll overlap with the "
"connection line. Drawing faster gives you wider curves areas."
msgstr ""
"Лінії кривих, які є додатковими уявними лініями, які формуються за кривими. "
"Програма створює лінії кривих, з'єднуючи одну точку на кривій із іншою, "
"попередньою, точкою на кривій. Це, серед іншого, означає, що у результаті "
"малювання прямої ви не побачите цих кривих ліній, оскільки вони "
"перекриватимуться із лінією з'єднання. Якщо ви робитимете мазок швидше, "
"криволінійні ділянки ставатимуть ширшими."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:35
msgid ".. image:: images/brushes/Krita-tutorial6-I.1-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.1-1.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:36
msgid ""
"You have access to 3 settings from the Lines tab, as well as 2 corresponding "
"dynamics:"
msgstr ""
"Ви маєте доступ до 3 параметрів на вкладці :guilabel:`Лінії`, а також 2 "
"відповідних динамічних характеристик:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:38
msgid ""
"Line width: this applies to both the connection line and the curve lines."
msgstr ""
"Товщина лінії: цей параметр стосується і лінії з'єднання і ліній кривих."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:40
msgid "Line width dynamics: use this to vary line width dynamically."
msgstr ""
"Динаміка товщини лінії: скористайтеся цим параметром для визначення "
"параметрів динамічної зміни товщини лінії."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:42
msgid ""
"History size: this determines the distance for the formation of curve lines."
msgstr ""
"Розмір журналу: цей параметр визначає відстань для формування ліній кривої."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:44
msgid ""
"If you set this at low values, then the curve lines can only form over a "
"small distances, so they won't be too visible."
msgstr ""
"Якщо ви встановите низьке значення, лінії кривої формуватимуться лише на "
"малих відстанях, отже будуть не дуже помітними."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:45
msgid ""
"On the other hand, if you set this value too high, the curve lines will only "
"start forming relatively \"late\"."
msgstr ""
"З іншого боку, якщо ви встановите надто велике значення, лінії кривих "
"почнуть створюватися відносно «пізно»."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:46
msgid ""
"So in fact, you'll get maximum curve lines area with a mid-value of say... "
"40~60, which is about the default value. Unless you're drawing at really "
"high resolutions."
msgstr ""
"Отже, фактично, ви отримаєте максимальну криволінійну ділянку із середнім "
"значенням, скажімо, 40-60, близьким до типового. Якщо, звичайно, ви не "
"малюєте із дуже високою роздільною здатністю."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:48
msgid ""
"Curves opacity: you can't set different line widths for the connection line "
"and the curve lines, but you can set a different opacity for the curve "
"lines. With low opacity, this will produce the illusion of thinner curve "
"lines."
msgstr ""
"Непрозорість кривих: ви не можете встановлювати різні товщини ліній для "
"лінії з'єднання та ліній кривих, але ви можете встановити інший рівень "
"непрозорості для ліній кривих. Менші значення непрозорості створюватимуть "
"враження тонших ліній кривих."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:50
msgid "Curves opacity dynamics: use this to vary Curves opacity dynamically."
msgstr ""
"Динаміка непрозорості кривих: цим параметром можна скористатися для "
"визначення параметрів динамічної зміни непрозорості кривих."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:52
msgid "In addition, you have access to two checkboxes:"
msgstr "Крім того, ви маєте доступ до двох пунктів для позначення:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:54
msgid ""
"Paint connection line, which toggles the visibility of the connection line"
msgstr ""
":guilabel:`Лінія з’єднання малюнка` перемикає видимість лінії з'єднання"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:55
msgid ""
"Smoothing, which... I have no idea actually. I don't see any differences "
"with or without it. Maybe it's for tablets?"
msgstr ""
":guilabel:`Згладжування`, яке... Ну, навіть не знаю. Не бачу ніяких "
"відмінностей між варіантом із позначеним і непозначеним пунктом. Можливо, "
"він для планшетів?"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:58
msgid ".. image:: images/brushes/Krita-tutorial6-I.1-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.1-2.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:60
msgid "Drawing variable-width lines"
msgstr "Малювання ліній змінної товщини"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:62
msgid ""
"And here's the only section of this tutorial that anyone cares about: pretty "
"lineart lines! For this:"
msgstr ""
"Ось і розділ цього підручника, заради якого ви усе це і читали: чудові лінії "
"графіки! Щоб досягти цього:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:64
msgid ""
"Use the Draw Dynamically mode: I tend to increase drag to at least 50. Vary "
"Mass and Drag until you get the feel that's most comfortable for you."
msgstr ""
"Скористайтеся режимом динамічного малювання. Варто збільшити значення "
"інерції до принаймні 50. Скоригуйте масу та інерцію, аж доки ви не отримаєте "
"найкомфортніший варіант для себе."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:67
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-1.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:68
msgid ""
"Set line width to a higher value (ex.: 5), then turn line width dynamics on:"
msgstr ""
"Встановіть товщину лінії у більше значення (приклад: 5), потім увімкніть "
"динаміку товщини лінії:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:70
msgid ""
"If you're a tablet user, just set this to Pressure (this should be selected "
"by default so just turn on the Line Width dynamics). I can't check myself, "
"but a tablet user confirmed to me that it works well enough with Draw "
"Dynamically."
msgstr ""
"Якщо ви користуєтеся планшетом, пов'яжіть це значення із тиском (це типовий "
"зв'язок, тому можете просто увімкнути динаміку товщині лінії). Це має "
"працювати достатньо добре із динамічним малюванням."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:71
msgid ""
"If you're a mouse user hoping to get variable line width, set the Line Width "
"dynamics to Speed."
msgstr ""
"Якщо ви користуєтеся мишею і хочете отримати змінну товщину лінії, "
"встановіть динаміку товщини лінії у значення :guilabel:`Швидкість`."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:74
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-2.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:75
msgid ""
"Set Curves opacity to 0: This is the simplest way to turn off the Curve "
"lines. That said, leaving them on will get you more \"expressive\" lines."
msgstr ""
"Встановіть непрозорість кривих у значення 0: це найпростіший спосіб "
"вимикання ліній кривих. Інакше кажучи, лишивши їх увімкненими ви отримаєте "
"«виразніші» лінії."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:78
msgid "Additional tips:"
msgstr "Додаткові підказки:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:80
msgid "Zig-zag a lot if you want a lot of extra curves lines."
msgstr "Рухайте пензель зигзагом, якщо хочете багато додаткових кривих ліній."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:81
msgid ""
"Use smooth, sweeping motions when you're using Draw Dynamically with Line "
"Width set to Speed: abrupt speed transitions will cause abrupt size "
"transitions. It takes a bit of practice, and the thicker the line, the more "
"visible the deformities will be. Also, zoom in to increase control."
msgstr ""
"Використовуйте плавні підмітальні рухи, коли користуєтеся динамічним "
"малюванням, де для товщини лінії встановлено зв'язок із датчиком швидкості: "
"різкі зміни швидкості призводитимуть до різких переходів у розмірах. Треба "
"буде трохи попрактикуватися, — чим товстішою буде лінія, тим помітнішим буде "
"її деформування. Крім того, можете збільшити масштаб, щоб працювати точніше."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:82
msgid ""
"If you need to vary between thin and thick lines, I suggest creating presets "
"of different widths, since you can't vary the base line width from the "
"canvas."
msgstr ""
"Якщо вам потрібно використовувати для малюнку тонкі і товсті лінії, "
"пропонуємо вам створити набори пензлів для різних товщин, оскільки змінювати "
"базову товщину лінії на полотні ви не зможете."

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:84
msgid "Alternative:"
msgstr "Альтернативний підхід:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:86
msgid "Use the Draw Dynamically mode"
msgstr "Скористайтеся режимом динамічного малювання"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:87
msgid "Set Curves opacity to 100"
msgstr "Встановіть непрозорість кривих у значення 100"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:88
msgid "Optionally decrease History size to about 30"
msgstr ""
"Якщо хочете, зменшіть розмір журналу до значення, яке приблизно рівне 30"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:90
msgid ""
"The curve lines will fill out the area they cover completely, resulting in a "
"line with variable widths. Anyway, here are some comparisons:"
msgstr ""
"Лінії кривих заповнять ділянку, яку вони повністю покривають, створюючи "
"лінію змінної товщини. Ось декілька прикладів ліній для порівняння:"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:93
msgid ".. image:: images/brushes/Krita-tutorial6-I.2-3.png"
msgstr ".. image:: images/brushes/Krita-tutorial6-I.2-3.png"

#: ../../reference_manual/brushes/brush_engines/curve_engine.rst:94
msgid "And here are examples of what you can do with this brush:"
msgstr ""
"І ось декілька прикладів того, що ви можете намалювати за допомогою цього "
"пензля:"
