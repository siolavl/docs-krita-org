# Translation of docs_krita_org_reference_manual___create_new_document.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___create_new_document\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:47+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/create_new_document.rst:1
msgid ""
"A simple guide to the first basic steps of using Krita: creating and saving "
"an image."
msgstr ""
"Простий підручник із перших кроків користування Krita: створення і "
"збереження зображення."

#: ../../reference_manual/create_new_document.rst:18
msgid "Save"
msgstr "Зберегти"

#: ../../reference_manual/create_new_document.rst:18
msgid "Load"
msgstr "Завантажити"

#: ../../reference_manual/create_new_document.rst:18
msgid "New"
msgstr "Створити"

#: ../../reference_manual/create_new_document.rst:22
msgid "Create New Document"
msgstr "Створення документа"

#: ../../reference_manual/create_new_document.rst:24
msgid "A new document can be created as follows."
msgstr "Нижче наведено настанови зі створення документа."

#: ../../reference_manual/create_new_document.rst:26
msgid "Click on :guilabel:`File` from the application menu at the top."
msgstr ""
"Натисніть пункт :guilabel:`Файл` у меню програми, розташованому у верхній "
"частині вікна."

#: ../../reference_manual/create_new_document.rst:27
msgid ""
"Then click on :guilabel:`New`. Or you can do this by pressing the :kbd:`Ctrl "
"+ N` shortcut."
msgstr ""
"Далі, натисніть пункт :guilabel:`Створити`. Ви також можете скористатися "
"натисканням комбінації клавіш :kbd:`Ctrl + N`."

#: ../../reference_manual/create_new_document.rst:28
msgid "Now you will get a New Document dialog box as shown below:"
msgstr ""
"У відповідь ви маєте побачити діалогове вікно «Створення документа», яке "
"показано нижче:"

#: ../../reference_manual/create_new_document.rst:31
msgid ".. image:: images/Krita_newfile.png"
msgstr ".. image:: images/Krita_newfile.png"

#: ../../reference_manual/create_new_document.rst:32
msgid ""
"There are various sections in this dialog box which aid in creation of new "
"document, either using custom document properties or by using contents from "
"clipboard and templates. Following are the sections in this dialog box:"
msgstr ""
"У цьому діалоговому вікні є різні розділи, які допомагають у створенні "
"документа за допомогою або визначення нетипових властивостей документа, або "
"визначення параметрів документа на основі вмісту буфера обміну даними та "
"шаблонів. Нижче описано розділи цього діалогового вікна:"

#: ../../reference_manual/create_new_document.rst:37
msgid "Custom Document"
msgstr "Нетиповий документ"

#: ../../reference_manual/create_new_document.rst:39
msgid ""
"From this section you can create a document according to your requirements: "
"you can specify the dimensions, color model, bit depth, resolution, etc."
msgstr ""
"За допомогою цього розділу ви зможете створити документ за визначеними вами "
"параметрами: ви можете вказати розмірність зображення, модель кольорів, "
"глибину кольорів, роздільну здатність тощо."

#: ../../reference_manual/create_new_document.rst:42
msgid ""
"In the top-most field of the :guilabel:`Dimensions` tab, from the Predefined "
"drop-down you can select predefined pixel sizes and PPI (pixels per inch). "
"You can also set custom dimensions and the orientation of the document from "
"the input fields below the predefined drop-down. This can also be saved as a "
"new predefined preset for your future use by giving a name in the Save As "
"field and clicking on the Save button. Below we find the Color section of "
"the new document dialog box, where you can select the color model and the "
"bit-depth. Check :ref:`general_concept_color` for more detailed information "
"regarding color."
msgstr ""
"У верхньому полі вкладки :guilabel:`Розміри` зі спадного списку "
"«Стандартний» ви можете вибрати стандартні розміри у пікселях та відповідну "
"роздільність (у пікселях на дюйм). Ви також можете встановити нетипові "
"розміри та орієнтацію документа за допомогою полів для введення, "
"розташованих під спадним списком «Стандартний». Визначені вами значення "
"можна зберегти як новий стандартний набір для подальшого використання, "
"вказавши назву набору у полі «Зберегти як» і натиснувши кнопку «Зберегти». "
"Нижче розташовано розділ «Колір» діалогового вікна створення документа. За "
"допомогою цього розділу ви можете вибрати модель кольорів та бітову глибину. "
"Ознайомтеся із вмістом розділу :ref:`general_concept_color`, щоб дізнатися "
"більше про роботу з кольорами у програмі."

#: ../../reference_manual/create_new_document.rst:52
msgid ""
"On the :guilabel:`Content` tab, you can define a name for your new document. "
"This name will appear in the metadata of the file, and Krita will use it for "
"the auto-save functionality as well. If you leave it empty, the document "
"will be referred to as 'Unnamed' by default. You can select the background "
"color and the amount of layers you want in the new document. Krita remembers "
"the amount of layers you picked last time, so be careful."
msgstr ""
"За допомогою вкладки :guilabel:`Вміст` ви можете визначити назву нового "
"документа. Цю назву буде використано у метаданих файла, а також для "
"реалізації механізмів автоматичного збереження даних у Krita. Якщо ви не "
"заповните поле назви, типово, документ вважатиметься назвами «Без назви». Ви "
"можете вибрати колір тла та кількість шарів у новому документі. Krita "
"запам'ятовує визначену вами раніше кількість шарів, тому вам слід звернути "
"увагу на цей параметр, якщо ви хочете змінити цю кількість."

#: ../../reference_manual/create_new_document.rst:59
msgid ""
"Finally, there's a description box, useful to note down what you are going "
"to do."
msgstr ""
"Нарешті, передбачено поле опису, яким можна скористатися для додавання опису "
"ваших намірів щодо зображення."

#: ../../reference_manual/create_new_document.rst:62
msgid "Create From Clipboard"
msgstr "Створити на основі вмісту буфера обміну"

#: ../../reference_manual/create_new_document.rst:64
msgid ""
"This section allows you to create a document from an image that is in your "
"clipboard, like a screenshot. It will have all the fields set to match the "
"clipboard image."
msgstr ""
"За допомогою цього розділу ви можете створити документ на основі даних "
"зображення, які було збережено до буфера обміну даними, як на нашому знімку "
"вікна. Усі параметри новоствореного зображення буде визначено за даними з "
"буфера обміну."

#: ../../reference_manual/create_new_document.rst:69
msgid "Templates:"
msgstr "Шаблони:"

#: ../../reference_manual/create_new_document.rst:71
msgid ""
"These are separate categories where we deliver special defaults. Templates "
"are just .kra files which are saved in a special location, so they can be "
"pulled up by Krita quickly. You can make your own template file from any ."
"kra file, by using :menuselection:`File --> Create Template From Image` in "
"the top menu. This will add your current document as a new template, "
"including all its properties along with the layers and layer contents."
msgstr ""
"Це окремі категорії, де ми визначаємо певні типові значення. Шаблони є "
"простими файлами .kra, які зберігаються до особливого каталогу з метою "
"пришвидшення доступ до них Krita. Ви можете створити власний файл шаблона з "
"будь-якого файла .kra за допомогою пункту головного меню :menuselection:"
"`Файл --> Створити шаблон з зображення`. У результаті ваш поточний документ "
"буде додано як новий шаблон, включно з усіма його властивостями, зокрема із "
"шарами та вмістом шарів."

#: ../../reference_manual/create_new_document.rst:78
msgid ""
"Once you have created a new document according to your preference, you "
"should now have a white canvas in front of you (or whichever background "
"color you chose in the dialog)."
msgstr ""
"Після створення документа відповідно до ваших налаштувань у вас буде біле "
"полотно (або полотно із кольором тла, який ви вибрали у діалоговому вікні)."
