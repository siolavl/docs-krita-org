# Translation of docs_krita_org_reference_manual___main_menu___layers_menu.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___layers_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:28+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Layerstyle (2.9.5+)"
msgstr "Стиль шару (2.9.5+)"

#: ../../reference_manual/main_menu/layers_menu.rst:1
msgid "The layers menu in Krita."
msgstr "Меню шарів у Krita."

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "Convert"
msgstr "Перетворити"

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Transform"
msgstr "Перетворити"

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:134
msgid "Histogram"
msgstr "Гістограма"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Layers"
msgstr "Шари"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Cut Layer"
msgstr "Вирізати шар"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Copy Layer"
msgstr "Копіювати шар"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Paste Layer"
msgstr "Вставити шар"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Import"
msgstr "Імпортувати"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Export"
msgstr "Експортувати"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Metadata"
msgstr "Метадані"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Flatten"
msgstr "Зведення"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Layer Style"
msgstr "Стиль шару"

#: ../../reference_manual/main_menu/layers_menu.rst:16
msgid "Layers Menu"
msgstr "Меню «Шари»"

#: ../../reference_manual/main_menu/layers_menu.rst:18
msgid ""
"These are the topmenu options are related to Layer Management, check out :"
"ref:`that page <layers_and_masks>` first, if you haven't."
msgstr ""
"Це пункти основного меню, які пов'язано із керуванням шарами. Спочатку "
"ознайомтеся із :ref:`цією сторінкою <layers_and_masks>`, якщо ви цього ще не "
"зробили."

#: ../../reference_manual/main_menu/layers_menu.rst:20
msgid "Cut Layer (3.0+)"
msgstr "Вирізати шар (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:21
msgid "Cuts the whole layer rather than just the pixels."
msgstr "Вирізати увесь шар, а не лише пікселі."

#: ../../reference_manual/main_menu/layers_menu.rst:22
msgid "Copy Layer (3.0+)"
msgstr "Копіювати шар (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:23
msgid "Copy the whole layer rather than just the pixels."
msgstr "Копіювати увесь шар, а не лише пікселі."

#: ../../reference_manual/main_menu/layers_menu.rst:24
msgid "Paste Layer (3.0+)"
msgstr "Вставити шар (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:25
msgid "Pastes the whole layer if any of the top two actions have been taken."
msgstr ""
"Вставити увесь шар після виконання будь-якої з двох наведених вище дій."

#: ../../reference_manual/main_menu/layers_menu.rst:27
#: ../../reference_manual/main_menu/layers_menu.rst:41
#: ../../reference_manual/main_menu/layers_menu.rst:60
#: ../../reference_manual/main_menu/layers_menu.rst:78
#: ../../reference_manual/main_menu/layers_menu.rst:92
#: ../../reference_manual/main_menu/layers_menu.rst:102
#: ../../reference_manual/main_menu/layers_menu.rst:118
msgid "Organizes the following actions:"
msgstr "Містити такі підпункти:"

#: ../../reference_manual/main_menu/layers_menu.rst:29
msgid "Paint Layer"
msgstr "Шар малювання"

#: ../../reference_manual/main_menu/layers_menu.rst:30
msgid "Add a new paint layer."
msgstr "Додати новий шар малювання."

#: ../../reference_manual/main_menu/layers_menu.rst:31
msgid "New layer from visible (3.0.2+)"
msgstr "Створити шар з видимого (3.0.2+)"

#: ../../reference_manual/main_menu/layers_menu.rst:32
msgid "Add a new layer with the visible pixels."
msgstr "Додати новий шар із видимими пікселями."

#: ../../reference_manual/main_menu/layers_menu.rst:33
msgid "Duplicate Layer or Mask"
msgstr "Дублювати шар або маску"

#: ../../reference_manual/main_menu/layers_menu.rst:34
msgid "Duplicates the layer."
msgstr "Дублювати шар."

#: ../../reference_manual/main_menu/layers_menu.rst:35
msgid "Cut Selection to New Layer"
msgstr "Вирізати позначене до нового шару"

#: ../../reference_manual/main_menu/layers_menu.rst:36
msgid "Single action for cut+paste."
msgstr "Об'єднаний пункт для дії вирізати+вставити."

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "New"
msgstr "Створити"

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "Copy Selection to New Layer"
msgstr "Скопіювати позначене до нового шару"

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "Single action for copy+paste."
msgstr "Об'єднаний пункт для дії копіювати+вставити."

#: ../../reference_manual/main_menu/layers_menu.rst:43
msgid "Save Layer or Mask"
msgstr "Зберегти шар або маску"

#: ../../reference_manual/main_menu/layers_menu.rst:44
msgid "Saves the Layer or Mask as a separate image."
msgstr "Зберегти шар або маску як окреме зображення."

#: ../../reference_manual/main_menu/layers_menu.rst:45
msgid "Save Vector Layer as SVG"
msgstr "Зберегти векторний шар як SVG"

#: ../../reference_manual/main_menu/layers_menu.rst:46
msgid "Save the currently selected vector layer as an SVG."
msgstr "Зберегти поточний позначений векторний шар як SVG."

#: ../../reference_manual/main_menu/layers_menu.rst:47
msgid "Save Group Layers"
msgstr "Зберегти групи шарів"

#: ../../reference_manual/main_menu/layers_menu.rst:48
msgid "Saves the top-level group layers as single-layer images."
msgstr "Зберегти групу шарів верхнього рівня як зображення із одним шаром."

#: ../../reference_manual/main_menu/layers_menu.rst:49
msgid "Import Layer"
msgstr "Імпортувати шар"

#: ../../reference_manual/main_menu/layers_menu.rst:50
msgid "Import an image as a layer into the current file."
msgstr "Імпортувати зображення як шар до поточного файла."

#: ../../reference_manual/main_menu/layers_menu.rst:52
msgid ""
"Import an image as a specific layer type. The following layer types are "
"supported:"
msgstr ""
"Імпортувати зображення як шар вказаного типу. Передбачено підтримку таких "
"типів шарів:"

#: ../../reference_manual/main_menu/layers_menu.rst:54
msgid "Paint layer"
msgstr "Шар малювання"

#: ../../reference_manual/main_menu/layers_menu.rst:55
#: ../../reference_manual/main_menu/layers_menu.rst:66
msgid "Transparency Mask"
msgstr "Маска прозорості"

#: ../../reference_manual/main_menu/layers_menu.rst:56
#: ../../reference_manual/main_menu/layers_menu.rst:68
msgid "Filter Mask"
msgstr "Маска фільтрування"

#: ../../reference_manual/main_menu/layers_menu.rst:57
msgid "Import/Export"
msgstr "Імпортування та експортування"

#: ../../reference_manual/main_menu/layers_menu.rst:57
msgid "Import as..."
msgstr "Імпортувати як…"

#: ../../reference_manual/main_menu/layers_menu.rst:57
#: ../../reference_manual/main_menu/layers_menu.rst:70
msgid "Selection Mask"
msgstr "Маска вибору"

#: ../../reference_manual/main_menu/layers_menu.rst:62
msgid "Convert a layer to..."
msgstr "Перетворити шар на…"

#: ../../reference_manual/main_menu/layers_menu.rst:64
msgid "Convert to Paint Layer"
msgstr "Перетворити на шар малювання"

#: ../../reference_manual/main_menu/layers_menu.rst:65
msgid "Convert a mask or vector layer to a paint layer."
msgstr "Перетворити маску або векторний шар на шар малювання."

#: ../../reference_manual/main_menu/layers_menu.rst:67
msgid ""
"Convert a layer to a transparency mask. The image will be converted to "
"grayscale first, and these grayscale values are used to drive the "
"transparency."
msgstr ""
"Перетворити шар на маску прозорості. Зображення спочатку буде перетворено на "
"зображення у відтінках сірого, а потім ці значення відтінків сірого буде "
"використано для створення прозорості."

#: ../../reference_manual/main_menu/layers_menu.rst:69
msgid ""
"Convert a layer to a filter mask. The image will be converted to grayscale "
"first, and these grayscale values are used to drive the filter effect area."
msgstr ""
"Перетворити шар на маску фільтрування. Зображення спочатку буде перетворено "
"на зображення у відтінках сірого, потім отримані значення буде використано "
"для визначення області застосування ефекту фільтрування."

#: ../../reference_manual/main_menu/layers_menu.rst:71
msgid ""
"Convert a layer to a selection mask. The image will be converted to "
"grayscale first, and these grayscale values are used to drive the selected "
"area."
msgstr ""
"Перетворити шар на маску вибору. Зображення спочатку буде перетворено на "
"зображення у відтінках сірого, а потім ці значення відтінків сірого буде "
"використано для визначення області позначення."

#: ../../reference_manual/main_menu/layers_menu.rst:72
msgid "Convert Group to Animated Layer"
msgstr "Перетворити групу на анімований шар"

#: ../../reference_manual/main_menu/layers_menu.rst:73
msgid ""
"This takes the images in the group layer and makes them into frames of an "
"animated layer."
msgstr ""
"Використати зображення у групі шарів і зробити з них кадри анімованого шару."

#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "Convert Layer Color Space"
msgstr "Перетворити простір кольорів шару"

#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "This only converts the color space of the layer, not the image."
msgstr "Перетворити простір кольорів шару, а не усього зображення."

#: ../../reference_manual/main_menu/layers_menu.rst:80
msgid "All layers"
msgstr "Всі шари"

#: ../../reference_manual/main_menu/layers_menu.rst:81
msgid "Select all layers."
msgstr "Позначити усі шари."

#: ../../reference_manual/main_menu/layers_menu.rst:82
msgid "Visible Layers"
msgstr "Видимі шари"

#: ../../reference_manual/main_menu/layers_menu.rst:83
msgid "Select all visible layers."
msgstr "Позначити усі видимі шари."

#: ../../reference_manual/main_menu/layers_menu.rst:84
msgid "Invisible Layers"
msgstr "Невидимі шари"

#: ../../reference_manual/main_menu/layers_menu.rst:85
msgid "Select all invisible layers, useful for cleaning up a sketch."
msgstr "Позначити усі невидимі шари. Корисно для чищення ескіза."

#: ../../reference_manual/main_menu/layers_menu.rst:86
msgid "Locked Layers"
msgstr "Заблоковані шари"

#: ../../reference_manual/main_menu/layers_menu.rst:87
msgid "Select all locked layers."
msgstr "Позначити усі заблоковані шари."

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Select (3.0+):"
msgstr "Вибрати (3.0+):"

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Unlocked Layers"
msgstr "Незаблоковані шари"

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Select all unlocked layers."
msgstr "Позначити усі незаблоковані шари."

#: ../../reference_manual/main_menu/layers_menu.rst:94
msgid "Quick Group (3.0+)"
msgstr "Швидко згрупувати (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:95
msgid "Adds all selected layers to a group."
msgstr "Додати усі позначені шари до групи."

#: ../../reference_manual/main_menu/layers_menu.rst:96
msgid "Quick Clipping Group (3.0+)"
msgstr "Групувати із швидким обрізанням (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:97
msgid ""
"Adds all selected layers to a group and adds a alpha-inherited layer above "
"it."
msgstr ""
"Додати позначені шари до групи і додати шар успадкованої прозорості над ними."

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Group"
msgstr "Гурпа"

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Quick Ungroup"
msgstr "Швидко розгрупувати"

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Ungroups the activated layer."
msgstr "Розгрупувати активований шар."

#: ../../reference_manual/main_menu/layers_menu.rst:104
msgid "Mirror Layer Horizontally"
msgstr "Віддзеркалити шар горизонтально"

#: ../../reference_manual/main_menu/layers_menu.rst:105
msgid "Mirror the layer horizontally using the image center."
msgstr "Віддзеркалити шар горизонтально відносного центра зображення."

#: ../../reference_manual/main_menu/layers_menu.rst:106
msgid "Mirror Layer Vertically"
msgstr "Віддзеркалити шар вертикально"

#: ../../reference_manual/main_menu/layers_menu.rst:107
msgid "Mirror the layer vertically using the image center."
msgstr "Віддзеркалити шар вертикально відносного центра зображення."

#: ../../reference_manual/main_menu/layers_menu.rst:108
msgid "Rotate"
msgstr "Обернути"

#: ../../reference_manual/main_menu/layers_menu.rst:109
msgid "Rotate the layer around the image center."
msgstr "Обернути шар відносно центра зображення."

#: ../../reference_manual/main_menu/layers_menu.rst:110
msgid "Scale Layer"
msgstr ""
"Масштабувати шар\n"
"масштабування шару"

#: ../../reference_manual/main_menu/layers_menu.rst:111
msgid ""
"Scale the layer by the given amounts using the given interpolation filter."
msgstr ""
"Масштабувати шар за вказаним значенням, використовуючи заданий фільтр "
"інтерполяції."

#: ../../reference_manual/main_menu/layers_menu.rst:112
msgid "Shear Layer"
msgstr "Перекосити шар"

#: ../../reference_manual/main_menu/layers_menu.rst:113
msgid "Shear the layer pixels by the given X and Y angles."
msgstr "Перекосити пікселі шару за вказаними кутами повороту X та Y."

#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Offset Layer"
msgstr "Змістити шар"

#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Offset the layer pixels by a given amount."
msgstr "Зсунути пікселі шару на вказаний вектор."

#: ../../reference_manual/main_menu/layers_menu.rst:120
msgid "Split Alpha"
msgstr "Розділити прозорість"

#: ../../reference_manual/main_menu/layers_menu.rst:121
msgid ""
"Split the image transparency into a mask. This is useful when you wish to "
"edit the transparency separately."
msgstr ""
"Відокремити прозорі області зображення у маску. Корисно, якщо ви хочете "
"виконати редагування прозорих ділянок окремо."

#: ../../reference_manual/main_menu/layers_menu.rst:122
msgid "Split Layer"
msgstr "Поділ шару"

#: ../../reference_manual/main_menu/layers_menu.rst:123
msgid ":ref:`Split the layer <split_layer>` into given color fields."
msgstr ":ref:`Розділити шар <split_layer>` на задані поля кольорів."

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid "Split..."
msgstr "Розділити…"

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid "Clones Array"
msgstr "Масив клонів"

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid ""
"A complex bit of functionality to generate clone-layers for quick sprite "
"making. See :ref:`clones_array` for more details."
msgstr ""
"Складна функціональна можливість для створення шарів-клонів для пришвидшення "
"створення спрайтів. Див. :ref:`clones_array`, щоб дізнатися більше."

#: ../../reference_manual/main_menu/layers_menu.rst:127
msgid "Edit Metadata"
msgstr "Змінити метадані"

#: ../../reference_manual/main_menu/layers_menu.rst:128
msgid "Each layer can have its own metadata."
msgstr "У кожного шару можуть бути власні метадані."

#: ../../reference_manual/main_menu/layers_menu.rst:130
msgid "Shows a histogram."
msgstr "Показати гістограму."

#: ../../reference_manual/main_menu/layers_menu.rst:134
msgid "Removed. Use the :ref:`histogram_docker` instead."
msgstr ""
"Вилучено. Скористайтеся замість цього інструментом :ref:`histogram_docker`."

#: ../../reference_manual/main_menu/layers_menu.rst:136
msgid "Merge With Layer Below"
msgstr "Об'єднати з шаром нижче"

#: ../../reference_manual/main_menu/layers_menu.rst:137
msgid "Merge a layer down."
msgstr "Об'єднати шар із нижнім."

#: ../../reference_manual/main_menu/layers_menu.rst:138
msgid "Flatten Layer"
msgstr "Звести шар"

#: ../../reference_manual/main_menu/layers_menu.rst:139
msgid "Flatten a Group Layer or flatten the masks into any other layer."
msgstr "Звести групу шарів або звести маски до будь-якого іншого шару."

#: ../../reference_manual/main_menu/layers_menu.rst:140
msgid "Rasterize Layer"
msgstr "Растеризувати шар"

#: ../../reference_manual/main_menu/layers_menu.rst:141
msgid "For making vectors into raster layers."
msgstr "Для перетворення векторних шарів на растрові."

#: ../../reference_manual/main_menu/layers_menu.rst:142
msgid "Flatten Image"
msgstr ""
"Звести зображення\n"
"зведення зображення"

#: ../../reference_manual/main_menu/layers_menu.rst:143
msgid "Flatten all layers into one."
msgstr "Зводить усі шари у один."

#: ../../reference_manual/main_menu/layers_menu.rst:145
msgid "Set the PS-style layerstyle."
msgstr "Встановити стиль шарів у форматі Photoshop."
