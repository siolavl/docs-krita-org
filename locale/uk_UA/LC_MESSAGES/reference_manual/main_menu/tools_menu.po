# Translation of docs_krita_org_reference_manual___main_menu___tools_menu.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___tools_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:37+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/main_menu/tools_menu.rst:1
msgid "The tools menu in Krita."
msgstr "Меню інструментів у Krita."

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Macro"
msgstr "Макрос"

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Scripts"
msgstr "Скрипти"

#: ../../reference_manual/main_menu/tools_menu.rst:17
msgid "Tools Menu"
msgstr "Меню «Інструменти»"

#: ../../reference_manual/main_menu/tools_menu.rst:19
msgid "This contains three things."
msgstr "У цьому меню міститься три пункти."

#: ../../reference_manual/main_menu/tools_menu.rst:22
msgid "Scripting"
msgstr "Скрипти"

#: ../../reference_manual/main_menu/tools_menu.rst:24
msgid ""
"When you have python scripting enabled and have scripts toggled, this is "
"where most scripts are stored by default."
msgstr ""
"Якщо ви увімкнули систему сценаріїв мовою Python і маєте сценарії цією "
"мовою, за допомогою цього пункту можна буде отримати доступ до більшості "
"сценаріїв."

#: ../../reference_manual/main_menu/tools_menu.rst:27
msgid "Recording"
msgstr "Запис"

#: ../../reference_manual/main_menu/tools_menu.rst:31
msgid "The recording and macro features are unmaintained and buggy."
msgstr ""
"Можливості запису та відтворення макросів є недостатньо розробленими і, "
"можливо, працюють не так, як нам би цього хотілося."

#: ../../reference_manual/main_menu/tools_menu.rst:33
msgid ""
"Record a macro. You do this by pressing start, drawing something and then "
"pressing stop. This feature can only record brush strokes. The resulting "
"file is stored as a \\*.kritarec file."
msgstr ""
"Записати макрос. Ви натискаєте кнопку «Пуск», щось малюєте, а потім "
"натискаєте кнопку «Стоп». Передбачено запис лише мазків пензлем. У "
"результаті обробки буде збережено файл макросу \\*.kritarec."

#: ../../reference_manual/main_menu/tools_menu.rst:36
msgid "Macros"
msgstr "Макроси"

#: ../../reference_manual/main_menu/tools_menu.rst:38
msgid ""
"Play back or edit a krita rec file. The edit can only change the brush "
"preset on strokes or add and remove filters."
msgstr ""
"Відтворити або редагувати файл rec Krita. Під час редагування можна "
"змінювати лише набір пензлів для мазків та додавати або вилучати фільтри."
