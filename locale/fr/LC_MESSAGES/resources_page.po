msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-27 08:20+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-deevadBrushes.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-mirandaBrushes.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-conceptBrushes.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-aldyBrushes.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-vascoBrushes.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-meemodrawsBrushes.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-stalcryBrushes.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-woltheraBrushes.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-nylnook.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-hushcoilBrushes.png"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-raghukamathBrushes.png"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-GDQuestBrushes.jpeg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-iForce73Brushes.png"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-deevadTextures.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/Resources-deevadTextures2.jpg"
msgstr ""

#: ../../resources_page.rst:0
msgid ".. image:: images/resource_packs/simon_pixel_art_course.png"
msgstr ""

#: ../../resources_page.rst:1
msgid "Resource Packs for Krita."
msgstr ""

#: ../../resources_page.rst:18
msgid "Resources"
msgstr "Ressources"

#: ../../resources_page.rst:21
msgid "Brush Packs"
msgstr ""

#: ../../resources_page.rst:28 ../../resources_page.rst:87
#: ../../resources_page.rst:91
msgid "David Revoy"
msgstr "David Revoy"

#: ../../resources_page.rst:32
msgid "Ramon Miranda"
msgstr "Ramon Miranda"

#: ../../resources_page.rst:36
msgid "Concept art & Illustration Pack"
msgstr ""

#: ../../resources_page.rst:40
msgid "Al-dy"
msgstr ""

#: ../../resources_page.rst:44
msgid "Vasco Basqué"
msgstr ""

#: ../../resources_page.rst:48
msgid "Meemodraws"
msgstr ""

#: ../../resources_page.rst:52
msgid "Stalcry"
msgstr ""

#: ../../resources_page.rst:56
msgid "Wolthera"
msgstr ""

#: ../../resources_page.rst:60
msgid "Nylnook"
msgstr ""

#: ../../resources_page.rst:64
msgid "Hushcoil"
msgstr ""

#: ../../resources_page.rst:68
msgid "Raghukamath"
msgstr ""

#: ../../resources_page.rst:72
msgid "GDQuest"
msgstr ""

#: ../../resources_page.rst:80
msgid "Texture Packs"
msgstr ""

#: ../../resources_page.rst:94
msgid "External tutorials"
msgstr ""

#: ../../resources_page.rst:101
msgid "Simón Sanchez' \"Learn to Create Pixel Art from Zero\" course on Udemy"
msgstr ""

#: ../../resources_page.rst:104
msgid "User-made Python Plugins"
msgstr ""

#: ../../resources_page.rst:105
msgid ""
"To install and manage your plugins, visit the :ref:"
"`krita_python_plugin_howto` area. See the second area on how to get Krita to "
"recognize your plugin."
msgstr ""

#: ../../resources_page.rst:107
msgid "Direct Eraser Plugin"
msgstr ""

#: ../../resources_page.rst:109
msgid ""
"https://www.mediafire.com/file/sotzc2keogz0bor/Krita+Direct+Eraser+Plugin.zip"
msgstr ""

#: ../../resources_page.rst:111
msgid "Tablet Controls Docker"
msgstr ""

#: ../../resources_page.rst:113
msgid "https://github.com/tokyogeometry/tabui"
msgstr ""

#: ../../resources_page.rst:115
msgid "On-screen Canvas Shortcuts"
msgstr ""

#: ../../resources_page.rst:117
msgid ""
"https://github.com/qeshi/henriks-onscreen-krita-shortcut-buttons/tree/master/"
"henriks_krita_buttons"
msgstr ""

#: ../../resources_page.rst:119
msgid "Spine File Format Export"
msgstr ""

#: ../../resources_page.rst:121
msgid "https://github.com/chartinger/krita-unofficial-spine-export"
msgstr ""

#: ../../resources_page.rst:123
msgid "GDQuest - Designer Tools"
msgstr ""

#: ../../resources_page.rst:125
msgid "https://github.com/GDquest/Krita-designer-tools"
msgstr ""

#: ../../resources_page.rst:127
msgid "AnimLayers (Animate with Layers)"
msgstr ""

#: ../../resources_page.rst:129
msgid "https://github.com/thomaslynge/krita-plugins"
msgstr ""

#: ../../resources_page.rst:131
msgid "Art Revision Control (using GIT)"
msgstr ""

#: ../../resources_page.rst:133
msgid "https://github.com/abeimler/krita-plugin-durra"
msgstr ""

#: ../../resources_page.rst:135
msgid "Krita Plugin generator"
msgstr ""

#: ../../resources_page.rst:137
msgid "https://github.com/cg-cnu/vscode-krita-plugin-generator"
msgstr ""

#: ../../resources_page.rst:139
msgid "Bash Action (works with OSX and Linux)"
msgstr ""

#: ../../resources_page.rst:141
msgid ""
"https://github.com/juancarlospaco/krita-plugin-bashactions#krita-plugin-"
"bashactions"
msgstr ""

#: ../../resources_page.rst:143
msgid "Reference Image Docker (old style)"
msgstr ""

#: ../../resources_page.rst:145
msgid "https://github.com/antoine-roux/krita-plugin-reference"
msgstr ""

#: ../../resources_page.rst:147
msgid "Post images on Mastadon"
msgstr ""

#: ../../resources_page.rst:149
msgid "https://github.com/spaceottercode/kritatoot"
msgstr ""

#: ../../resources_page.rst:151
msgid "Python auto-complete for text editors"
msgstr ""

#: ../../resources_page.rst:153
msgid "https://github.com/scottpetrovic/krita-python-auto-complete"
msgstr ""

#: ../../resources_page.rst:157
msgid "See Something We Missed?"
msgstr ""

#: ../../resources_page.rst:158
msgid ""
"Have a resource you made and want to to share it with other artists? Let us "
"know in the forum or visit our chat room to discuss getting the resource "
"added to here."
msgstr ""
