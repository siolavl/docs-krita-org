# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:32+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/oncanvas_brush_editor.rst:1
msgid "Using the oncanvas brush editor in Krita."
msgstr "Användning av penseleditorn på duken i Krita."

#: ../../user_manual/oncanvas_brush_editor.rst:11
msgid "Brush Settings"
msgstr "Penselinställningar"

#: ../../user_manual/oncanvas_brush_editor.rst:11
msgid "Pop-up Palette"
msgstr "Palettruta"

#: ../../user_manual/oncanvas_brush_editor.rst:16
msgid "On-Canvas Brush Editor"
msgstr "Penseleditor på duken"

#: ../../user_manual/oncanvas_brush_editor.rst:19
msgid ""
"Krita's brush editor is, as you may know, on the :kbd:`F5` key. However, "
"sometimes you just want to modify a single parameter quickly. Perhaps even "
"in canvas-only mode. The on canvas brush editor or brush HUD allows you to "
"do this. It's accessible from the pop-up palette, by ticking the lower-right "
"arrow button."
msgstr ""
"Kritas penseleditor finns, som du kanske redan vet, på tangenten :kbd:`F5`. "
"Dock vill man ibland bara ändra en enstaka parameter snabbt, kanske till och "
"med när bara duken visas. Penseleditorn på duken, eller "
"penselsiktlinjesindikatorn möjliggör det. Den är tillgänglig från "
"palettrutan, genom att klicka på pilknappen längst ner till höger."

#: ../../user_manual/oncanvas_brush_editor.rst:26
msgid ".. image:: images/On_canvas_brush_editor.png"
msgstr ".. image:: images/On_canvas_brush_editor.png"

#: ../../user_manual/oncanvas_brush_editor.rst:27
msgid ""
"You can change the amount of visible settings and their order by clicking "
"the settings icon next to the brush name."
msgstr ""
"Antalet synliga inställningar och deras ordning kan ändras genom att klicka "
"på inställningsikonen intill penselnamnet."

#: ../../user_manual/oncanvas_brush_editor.rst:31
msgid ".. image:: images/On_canvas_brush_editor_2.png"
msgstr ".. image:: images/On_canvas_brush_editor_2.png"

#: ../../user_manual/oncanvas_brush_editor.rst:32
msgid ""
"On the left are all unused settings, on the right are all used settings. You "
"use the :guilabel:`>` and :guilabel:`<` buttons to move a setting between "
"the two columns. The :guilabel:`Up` and :guilabel:`Down` buttons allow you "
"to adjust the order of the used settings, for when you think flow is more "
"important than size."
msgstr ""
"Till vänster finns alla oanvända inställningar, och till höger finns använda "
"inställningar. Man kan använda knapparna :guilabel:`>` och :guilabel:`<` för "
"att flytta en inställning mellan de två kolumnerna. Knapparna :guilabel:"
"`Upp` och :guilabel:`Ner` tillåter att ordningen av använda inställningar "
"ändras, när flödet anses viktigare än storleken."

#: ../../user_manual/oncanvas_brush_editor.rst:38
msgid ".. image:: images/On_canvas_brush_editor_3.png"
msgstr ".. image:: images/On_canvas_brush_editor_3.png"

#: ../../user_manual/oncanvas_brush_editor.rst:39
msgid ""
"These set-ups are PER brush engine, so different brush engines can have "
"different configurations."
msgstr ""
"Inställningarna gäller per penselgränssnitt, så olika penselgränssnitt kan "
"ha olika inställningar."
