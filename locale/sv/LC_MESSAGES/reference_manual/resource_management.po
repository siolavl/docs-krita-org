# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:15+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:8
msgid ""
".. image:: images/icons/Krita_mouse_scroll.png\n"
"   :alt: mousescroll"
msgstr ""
".. image:: images/icons/Krita_mouse_scroll.png\n"
"   :alt: mushjul"

#: ../../reference_manual/resource_management.rst:None
msgid ".. image:: images/resources/Tags-krita.png"
msgstr ".. image:: images/resources/Tags-krita.png"

#: ../../reference_manual/resource_management.rst:None
msgid ".. image:: images/resources/Manageresources.png"
msgstr ".. image:: images/resources/Manageresources.png"

#: ../../reference_manual/resource_management.rst:1
msgid "Overview of Resource Management in Krita."
msgstr "Översikt av resurshantering i Krita."

#: ../../reference_manual/resource_management.rst:11
#: ../../reference_manual/resource_management.rst:21
msgid "Bundles"
msgstr "Packar"

#: ../../reference_manual/resource_management.rst:11
msgid "Resources"
msgstr "Resurser"

#: ../../reference_manual/resource_management.rst:16
msgid "Resource Management"
msgstr "Resurshantering"

#: ../../reference_manual/resource_management.rst:18
msgid ""
"Resources are pluggable bits of data, like brush presets or patterns. Krita "
"has variety of resources and also has a good resource management starting "
"from 2.9, making it really easy for artists to share and collate all the "
"resources together"
msgstr ""
"Resurser är inkopplingsbara databitar, som penselförinställningar eller "
"mönster. Krita har en mängd resurser och har också bra resurshantering med "
"början på 2.9, vilket gör det enkelt för konstnärer att dela och "
"sammanställa alla resurser."

#: ../../reference_manual/resource_management.rst:23
msgid ""
"Starting from 2.9 Krita has a new format to manage resources it is called "
"''Bundles'', a bundle is just a compressed file containing all the resources "
"together."
msgstr ""
"Med början på 2.9 har Krita ett nytt format för att hantera resurser, som "
"kallas \"packar\". En packe är bara en komprimerad fil som innehåller alla "
"resurserna."

#: ../../reference_manual/resource_management.rst:26
msgid "Tags"
msgstr "Etiketter"

#: ../../reference_manual/resource_management.rst:28
msgid ""
"Krita also has a robust tagging system for you to manage the resources on "
"the fly while painting. All Krita resources can be tagged. These tags can be "
"added via the resource manager, but also via the respective dockers such as "
"brush preset docker, pattern docker etc. You can |mouseleft| the plus icon "
"in the docker and add a tag name. In addition to adding you can rename and "
"delete a tag as well."
msgstr ""
"Krita har också ett robust etikettsystem för att hantera resurserna i farten "
"vid målning. Alla resurser i Krita kan etiketteras. Etiketterna kan läggas "
"till med resurshanteraren, men också via respektive paneler såsom "
"penselförinställningspanelen, mönsterpanelen, etc. Man kan vänsterklicka på "
"plusikonen i panelen och lägga till ett etikettnamn. Förutom att lägga till "
"det kan man också byta namn på eller ta bort en etikett."

#: ../../reference_manual/resource_management.rst:33
msgid ""
"Resources can belong to one or more tags.  For example, you may have a Brush "
"Preset of a favorite **Ink Pen** variant and have it tagged so it shows in "
"up in your Inking, Painting, Comics and Favorites groups of brushes."
msgstr ""
"Resurser kan höra till en eller flera etiketter. Man kan exempelvis ha en "
"penselförinställning för en favoritvariant av **bläckpenna** och etikettera "
"den så att den dyker upp i penselgrupperna för teckning, målning, serier och "
"favoriter."

#: ../../reference_manual/resource_management.rst:34
msgid ""
"Brushes in the \"Predefined\" tab of the Brush Settings Editor can be also "
"tagged and grouped for convenience."
msgstr ""
"Penslar under fliken \"Fördefinierade\" i penselinställningseditorn kan "
"också etiketteras och grupperas för bekvämlighet."

#: ../../reference_manual/resource_management.rst:37
msgid "Filtering"
msgstr "Filtrering"

#: ../../reference_manual/resource_management.rst:40
msgid ""
"Some dockers, for example the brush preset docker as shown below, have a "
"resource filter, which functions like a powerful search bar for the resource "
"in question."
msgstr ""
"Visa paneler, exempelvis penselförinställningspanelen som visas nedan, har "
"ett resursfilter som fungerar som en kraftfull sökrad för resursen i fråga."

#: ../../reference_manual/resource_management.rst:43
msgid ".. image:: images/brushes/Brushpreset-filters.png"
msgstr ".. image:: images/brushes/Brushpreset-filters.png"

#: ../../reference_manual/resource_management.rst:44
msgid ""
"You can enter brush name, tag name to quickly pull up a list of brush "
"presets you want. When you select any tag from the tag drop-down and want to "
"include brush presets from other tags as well then you can add filters the "
"following way:"
msgstr ""
"Man kan skriva in penselnamn, etikettnamn eller snabbt ta fram en lista över "
"penselförinställningar som man vill ha. När man väljer en etikett i "
"etikettkombinationsrutan och dessutom vill inkludera penselförinställningar "
"från andra etiketter, kan man lägga till filter på följande sätt:"

#: ../../reference_manual/resource_management.rst:46
msgid ""
"To filter based on the partial, case insensitive name of the resources you "
"can add ``partialname`` or ``!partialname``."
msgstr ""
"För att filtrera baserat på det partiella skiftlägesokänsliga namnet på "
"resursen kan man lägga till ``partiellt namn`` eller ``!partiellt namn``."

#: ../../reference_manual/resource_management.rst:47
msgid ""
"To include other Tags type the respective name of the tag in square brackets "
"like this ``[Tagname]`` or to exclude a tag type ``![Tagname]``."
msgstr ""
"För att inkludera andra etiketter skriv in respektive namn på etiketten inom "
"hakparenteser så här ``[Etikettnamn]`` eller för att undanta en etikettyp ``!"
"[Etikettnamn]``."

#: ../../reference_manual/resource_management.rst:48
msgid ""
"For case sensitive matching of preset name type ``\"Preset name\"`` or ``! "
"\"Preset name\"`` to exclude."
msgstr ""
"För skiftlägeskänslig matchning av förinställningsnamn skriv ``"
"\"Förinställningsnamn\"`` eller ``! \"Förinställningsnamn\"`` för att "
"undanta det."

#: ../../reference_manual/resource_management.rst:51
msgid "An incredibly quick way to save a group or brushes into a tag is to:"
msgstr ""
"Ett otroligt snabbt sätt att spara en grupp eller penslar i en etikett är "
"att:"

#: ../../reference_manual/resource_management.rst:53
msgid ""
"Create a new tag by |mouseleft| on the green plus sign.  This will empty out "
"the contents of the Brush Preset docker."
msgstr ""
"Skapa en ny etikett genom att vänsterklicka på det gröna plustecknet. Det "
"tömmer innehållet i penselförinställningspanelen."

#: ../../reference_manual/resource_management.rst:54
msgid ""
"Use the :guilabel:`Resource Filter` at the bottom of the :guilabel:`Brush "
"Presets` dock or :guilabel:`Brush Settings Editor` to type in what you want "
"to group.  For instance: if you type **Pencil** in the filter box you will "
"get all Brush Presets with **Pencil** somewhere in their name.  Now you have "
"all the Pencil-related Brush Presets together in one place."
msgstr ""
"Använd :guilabel:`Resursfilter` längst ner i panelen :guilabel:"
"`Penselförinställningar` eller :guilabel:`Penselförinställningseditor` för "
"att skriva in vad som ska grupperas. Om man exempelvis skriver in **Penna** "
"i filterrutan får man alla penselförinställningar som har **Penna** "
"någonstans i sina namn. Nu har man alla pennrelaterade "
"penselförinställningarna tillsammans på et ställe."

#: ../../reference_manual/resource_management.rst:55
msgid ""
"To finish, click the :guilabel:`Save` button (small disk icon to the right "
"of the :guilabel:`Resource Filter` box) or press the :kbd:`Enter` key and "
"all the items will be saved with the new tag you created."
msgstr ""
"Klicka på knappen :guilabel:`Spara` (den lilla skivan till höger om rutan :"
"guilabel:`Resursfilter`) eller tryck på tangenten :kbd:`Enter` för att "
"slutföra det, så sparas alla objekt med den nya etiketten som man skapade."

#: ../../reference_manual/resource_management.rst:57
msgid ""
"Now, anytime you want to open up your \"digital pencil box\" and see what "
"you have to work with all you have to do is use the pull-down and select :"
"guilabel:`Pencils`.  The Resource Filter works the same way in other parts "
"of Krita so be on the lookout for it!"
msgstr ""
"När man nu när som helst vill öppna sin \"digitala pennlåda\" och se vad man "
"kan arbeta med, är allt man behöver göra att använda kombinationsrutan och "
"välja :guilabel:`Pennor`. Resursfiltret fungerar på samma sätt för andra "
"delar av Krita, så håll ögonen öppna!"

#: ../../reference_manual/resource_management.rst:60
msgid "Resource Zooming"
msgstr "Resurszoomning"

#: ../../reference_manual/resource_management.rst:60
msgid ""
"If you find the thumbnails of the resources such as color swatches brushes "
"and pattern to be small you can make them bigger or :guilabel:`Zoom in`. All "
"resource selectors can be zoomed in and out of, by hovering over the "
"selector and using the :kbd:`Ctrl +` |mousescroll| shortcut."
msgstr ""
"Om man tycker att resursernas miniatyrbilder, såsom färgrutor för penslar "
"och mönster, är för små, kan man göra dem större eller :guilabel:`Zooma in`. "
"Alla resursväljare kan zoomas in och ut genom att hålla musen över väljaren "
"och använda genvägen :kbd:`Ctrl +` mushjul."

#: ../../reference_manual/resource_management.rst:63
msgid "Managing Resources"
msgstr "Hantera resurser"

#: ../../reference_manual/resource_management.rst:65
msgid ""
"As mentioned earlier Krita has a flexible resource management system. "
"Starting from version 2.9 you can share various resources mentioned above by "
"sharing a single compressed zip file created within Krita."
msgstr ""
"Som tidigare nämnts har Krita ett flexibelt resurshanteringssystem. Med "
"början på version 2.9 kan man dela olika resurser som nämns ovan genom att "
"dela en enda komprimerad zip-fil skapad inne i Krita."

#: ../../reference_manual/resource_management.rst:67
msgid ""
"The manage resources section in the settings was also revamped for making it "
"easy for the artists to prepare these bundle files. You can open manage "
"resource section by going to :menuselection:`Settings`   then :menuselection:"
"`Manage Resources`."
msgstr ""
"Sektionen i inställningarna för att hantera resurser har också stöpts om för "
"att göra det enklare för konstnärer att göra i ordning packarna. Man kan "
"visa sektionen för resurshantering genom att gå till :menuselection:"
"`Inställningar` och därefter :menuselection:`Hantera resurser`."

#: ../../reference_manual/resource_management.rst:73
msgid "Importing Bundles"
msgstr "Importera packar"

#: ../../reference_manual/resource_management.rst:75
msgid ""
"To import a bundle click on :guilabel:`Import Bundles/Resources` button on "
"the top right side of the dialog. Select ``.bundle`` file format from the "
"file type if it is not already selected, browse to the folder where you have "
"downloaded the bundle, select it and click :guilabel:`Open`. Once the bundle "
"is imported it will be listed in the :guilabel:`Active Bundle` section. If "
"you don't need the bundle you can temporarily make it inactive by selecting "
"it and clicking on the arrow button, this will move it to the :guilabel:"
"`Inactive` section."
msgstr ""
"Klicka på knappen :guilabel:`Importera packar eller resurser` för att "
"importera en packe. Välj filformatet ``.bundle`` som filtyp om det inte "
"redan är valt, bläddra till katalogen dit packen har laddats ner, markera "
"den och klicka på :guilabel:`Öppna`. När packen väl har importerats listas "
"den i sektionen  :guilabel:`Aktiv packe`. Om man inte behöver packen kan den "
"tillfälligt göras inaktiv genom att markera den och klicka på piltangenten "
"så flyttas den till sektionen :guilabel:`Inaktiv`."

#: ../../reference_manual/resource_management.rst:79
msgid "Creating your own Bundle"
msgstr "Skapa en egen packe"

#: ../../reference_manual/resource_management.rst:81
msgid ""
"You can create your own bundle from the resources of your choice. Click on "
"the :guilabel:`Create bundle` button. This will open a dialog box as shown "
"below."
msgstr ""
"Man kan skapa en ny packe från önskad resurs. Klicka på knappen :guilabel:"
"`Skapa packe`. Det öppnar en dialogruta som visas nedan."

#: ../../reference_manual/resource_management.rst:84
msgid ".. image:: images/resources/Creating-bundle.png"
msgstr ".. image:: images/resources/Creating-bundle.png"

#: ../../reference_manual/resource_management.rst:85
msgid ""
"The left hand section is for filling up information about the bundle like "
"author name, website, email, bundle icon, etc. The right hand side provides "
"a list of available resources. Choose the type of resource you wish to add "
"in to the bundle from the drop-down above and add it to the bundle by "
"selecting a resource and clicking on the arrow button."
msgstr ""
"Sektionen på vänster sida är till för att fylla i information om packen, som "
"upphovsmannens namn, webbsida, e-post, packens ikon, etc. Höger sida "
"tillhandahåller en lista med tillgängliga resurser. Välj typ av resurs som "
"ska läggas till i packen från kombinationsrutan ovanför och lägg till den i "
"packen genom att markera en resurs och klicka på piltangenten."

#: ../../reference_manual/resource_management.rst:90
msgid ""
"Make sure you add brush tips for used in the respective paintop presets you "
"are adding to the bundle. If you don't provide the brush tips then the brush "
"presets loaded from this bundle will have a 'X' mark on the thumbnail "
"denoting that the texture is missing. And the brush preset won't be the same."
msgstr ""
"Säkerställ att penselspetsar läggs till för användning i respektive "
"förinställningar av målaroperationer som läggs till i packen. Om man inte "
"tillhandahåller penselspetsen kommer penselförinställningen som läses in "
"från packen ha en X-markering på miniatyrbilden som anger att strukturen "
"saknas. Och penselförinställningen är inte likadan."

#: ../../reference_manual/resource_management.rst:92
msgid ""
"Once you have added all the resources you can create bundle by clicking on "
"the :guilabel:`Save` button, the bundle will be saved in the location you "
"have specified. You can then share this bundle with other artists or load it "
"on other workstations."
msgstr ""
"När man väl har lagt till alla resurser kan packen skapas genom att klicka "
"på knappen  :guilabel:`Spara`, och packen sparas på angiven plats. Därefter "
"kan man dela med sig av packen till andra konstnärer eller läsa in den på "
"andra arbetsstationer."

#: ../../reference_manual/resource_management.rst:95
msgid "Deleting Backup files"
msgstr "Ta bort säkerhetskopior"

#: ../../reference_manual/resource_management.rst:97
msgid ""
"When you delete a preset from Krita, Krita doesn't actually delete the "
"physical copy of the preset. It just adds it to a black list so that the "
"next time when you start Krita the presets from this list are not loaded. To "
"delete the brushes from this list click on :guilabel:`Delete Backup Files`."
msgstr ""
"När en förinställning tas bort från Krita, raderar faktiskt Krita inte den "
"fysiska kopian av förinställningen. Istället läggs den till i en svartlista "
"så att när Krita startas nästa gång läses inte förinställningarna från den "
"här listan in. För att ta bort penslarna från listan, klicka på :guilabel:"
"`Ta bort säkerhetskopior`."

#: ../../reference_manual/resource_management.rst:100
msgid "Deleting Imported Bundles"
msgstr "Ta bort importerade packar"

#: ../../reference_manual/resource_management.rst:102
msgid ""
"In case you wish to delete the bundles you have imported permanently click "
"on the :guilabel:`Open Resource Folder` button in the :guilabel:`Manage "
"Resources` dialog. This will open the resource folder in your file manager / "
"explorer. Go inside the bundles folder and delete the bundle file which you "
"don't need any more. The next time you start Krita the bundle and its "
"associated resources will not be loaded."
msgstr ""
"I fall man vill permanent ta bort packar som man har importerat, klicka på "
"knappen  :guilabel:`Öppna resurskatalog` i dialogrutan  :guilabel:`Hantera "
"resurser`. Det öppnar resurskatalogen i filhanteraren eller utforskaren. Gå "
"in i katalogen med packar och ta bort filen för packen som inte längre "
"behövs. Nästa gång Krita startas läses inte packen och tillhörande resurs in."

#: ../../reference_manual/resource_management.rst:105
msgid "Resource Types in Krita"
msgstr "Resurstyper i Krita"
