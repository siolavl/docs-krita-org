# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:19+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Hard Edge"
msgstr "Hård kant"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:1
msgid "The Shape Brush Engine manual page."
msgstr "Manualsidan för formpenselgränssnittet."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:16
msgid "Shape Brush Engine"
msgstr "Formpenselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Experiment Brush Engine"
msgstr "Experimentellt penselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Al.Chemy"
msgstr "Al.Chemy"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:19
msgid ".. image:: images/icons/shapebrush.svg"
msgstr ".. image:: images/icons/shapebrush.svg"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:20
msgid "An Al.chemy inspired brush-engine. Good for making chaos with!"
msgstr "Ett penselgränssnitt inspirerat av Al.chemy. Bra för att skapa kaos!"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:23
msgid "Parameters"
msgstr "Parametrar"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:25
msgid ":ref:`option_experiment`"
msgstr ":ref:`option_experiment`"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:26
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:31
msgid "Experiment Option"
msgstr "Experimentellt alternativ"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:33
msgid "Speed"
msgstr "Hastighet"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:34
msgid ""
"This makes the outputted contour jaggy. The higher the speed, the jaggier."
msgstr "Gör utmatad kontur taggig. Ju högre hastighet desto taggigare."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:35
msgid "Smooth"
msgstr "Jämn"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:36
msgid ""
"Smoothens the output contour. This slows down the brush, but the higher the "
"smooth, the smoother the contour."
msgstr ""
"Jämnar ut utmatad kontur. Gör penseln långsammare, men ju större jämnhet "
"desto jämnare blir konturen."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:37
msgid "Displace"
msgstr "Förskjut"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:38
msgid ""
"This displaces the shape. The slow the movement, the higher the displacement "
"and expansion. Fast movements shrink the shape."
msgstr ""
"Förskjuter formen. Ju långsammare rörelse, desto större förskjutning och "
"expansion. Snabba rörelser krymper formen."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:39
msgid "Winding Fill"
msgstr "Fyll slingrande"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:40
msgid ""
"This gives you the option to use a 'non-zero' fill rules instead of the "
"'even-odd' fill rule, which means that where normally crossing into the "
"shape created transparent areas, it now will not."
msgstr ""
"Ger alternativet att använda ifyllnadsregeln 'icke-noll' istället för 'jämn-"
"udda', vilket betyder att om man normalt korsar in i genomskinliga områden "
"skapade av formen, gör man inte längre det."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:42
msgid "Removes the anti-aliasing, to get a pixelized line."
msgstr "Tar bort kantutjämning, för att få en bildpunktslinje."
