# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:21+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Preview"
msgstr "Förhandsgranskning"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:24
msgid ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: toolline"
msgstr ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: linjeverktyg"

#: ../../reference_manual/tools/line.rst:1
msgid "Krita's line tool reference."
msgstr "Referens för Kritas linjeverktyg."

#: ../../reference_manual/tools/line.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/line.rst:11
msgid "Line"
msgstr "Linje"

#: ../../reference_manual/tools/line.rst:11
msgid "Straight Line"
msgstr "Rät linje"

#: ../../reference_manual/tools/line.rst:16
msgid "Straight Line Tool"
msgstr "Rät linjeverktyg"

#: ../../reference_manual/tools/line.rst:18
msgid "|toolline|"
msgstr "|toolline|"

#: ../../reference_manual/tools/line.rst:21
msgid ""
"This tool is used to draw lines. Click the |mouseleft| to indicate the first "
"endpoint, keep the button pressed, drag to the second endpoint and release "
"the button."
msgstr ""
"Verktyget används för att rita linjer. Klicka på vänster musknapp för att "
"ange första ändpunkten, håll knappen nedtryckt, dra till den andra "
"ändpunkten och släpp knappen."

#: ../../reference_manual/tools/line.rst:24
msgid "Hotkeys and Sticky Keys"
msgstr "Snabbtangenter och klistriga tangenter"

#: ../../reference_manual/tools/line.rst:26
msgid ""
"To activate the Line tool from freehand brush mode, use the :kbd:`V` key. "
"Use other keys afterwards to constraint the line."
msgstr ""
"Använd tangenten :kbd:`V` för att aktivera linjeverktyget från "
"frihandspenselläget. Använd andra tangenter efteråt för att begränsa linjen."

#: ../../reference_manual/tools/line.rst:28
msgid ""
"Use the :kbd:`Shift` key while holding the mouse button to constrain the "
"angle to multiples of 15º. You can press the :kbd:`Alt` key while still "
"keeping the |mouseleft| down to move the line to a different location."
msgstr ""
"Använd tangenten :kbd:`Skift` medan musknappen för att begränsa vinkeln till "
"multipler av 15º. Man kan trycka på tangenten :kbd:`Alt` medan vänster "
"musknappen hålls nere för att flytta linjen till en annan plats."

#: ../../reference_manual/tools/line.rst:32
msgid ""
"Using the :kbd:`Shift` keys BEFORE pushing the holding the left mouse button/"
"stylus down will, in default Krita, activate the quick brush-resize. Be sure "
"to use the :kbd:`Shift` key after."
msgstr ""
"Genom att använda tangenten :kbd:`Skift` INNAN musknappen eller pennan "
"trycks ner, aktiverar man normalt snabbändringen av penselstorlek i Krita. "
"Säkerställ att tangenten :kbd:`Skift` används efteråt."

#: ../../reference_manual/tools/line.rst:35
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/line.rst:37
msgid ""
"The following options allow you to modify the end-look of the straight-line "
"stroke with tablet-values. Of course, this only work for tablets, and "
"currently only on Paint Layers."
msgstr ""
"Följande alternativ låter dig ändra det slutliga den räta linjens utseende "
"med värden från ritplattan. Naturligtvis fungerar det bara med ritplattor, "
"och för närvarande enbart på målarlager."

#: ../../reference_manual/tools/line.rst:40
msgid "Use sensors"
msgstr "Använd sensorer"

#: ../../reference_manual/tools/line.rst:41
msgid ""
"This will draw the line while taking sensors into account. To use this "
"effectively, start the line and trace the path like you would when drawing a "
"straight line before releasing. If you make a mistake, make the line shorter "
"and start over."
msgstr ""
"Ritar linjen med hänsyn tagen till sensorer. För att använda det effektivt, "
"starta linjen och följ konturen som du skulle göra när en rät linje ritas "
"innan du släpper. Om du gör ett misstag, gör linjen kortare och börja om."

#: ../../reference_manual/tools/line.rst:43
msgid ""
"This will show the old-fashioned preview line so you know where your line "
"will end up."
msgstr ""
"Visar en gammaldags förhandsgranskningsllinje så att man vet var linjen "
"hamnar till sist."
